import cgi, datetime
import urllib
from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db

class Sensdata(db.Model):

  sensorid = db.StringProperty()                     # can have multiple numbers
  msg = db.StringProperty()                          # message
  date = db.DateTimeProperty(auto_now_add=True)      # timestamp generated when the server recieves the data

class DataUpdate(webapp.RequestHandler):
    def get(self):
        sense=Sensdata()
        if self.request.get('sensorid'):
            sense.sensorid=self.request.get('sensorid')
        else:
            self.response.out.write('couldnt find sensorid')
        if self.request.get('msg'):
            sense.msg=self.request.get('msg')
        else:
            self.response.out.write('couldnt find msg')
        
        sense.put()
        self.response.out.write('ok')

    def post(self):                                  #get data by POST method
        sense=Sensdata()
        if self.request.get('sensorid'):
            #num=self.request.get('sensorid')
            #number=num.split(',')
            #number=number.strip('"')
            #sense.sensorid=number[1]
            sense.sensorid=self.request.get('sensorid')
        else:
            self.response.out.write('couldnt find sensorid')
        if self.request.get('msg'):
            sense.msg=self.request.get('msg')
        else:
            self.response.out.write('couldnt find msg')
        
        sense.put()
        self.response.out.write('ok')

#Homepage, shows sample data        
class MainPage(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<html><body>Welcome to sensdata!<p>Here are the last 10 messages:<p>')

        dispdata = db.GqlQuery("SELECT * FROM Sensdata ORDER BY date DESC LIMIT 10")
        
        for data in dispdata:
            if data.sensorid:
                if data.msg:
                    self.response.out.write('<b>%s</b> sensors message is'%data.sensorid)
                    self.response.out.write(' <b>%s</b> '%data.msg)
                                            
            else:
                self.response.out.write('some error')
        
            self.response.out.write('and was recieved at %s<p>'%data.date)

        self.response.out.write("</body></html>")

class NoData(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<html><body>Welcome to sensdata!<p>Here you can look at the messages of a specific no. </p>')
#get data from the form input below                
        num=self.request.get('num')
#query for the required data        
        dispdata = db.GqlQuery("SELECT * FROM Sensdata WHERE sensorid =:1 ORDER BY date DESC LIMIT 10",num)
        
        count=dispdata.count()              
        
        if count==0:                               # see if particular sensors' data is present
            self.response.out.write('<p> not present :P </p></body></html>')
        
        
        for data in dispdata:                      # display relevant data
            if data.sensorid:
                if data.msg:
                    self.response.out.write('<b>%s</b> numbers message is'%data.sensorid)
                    self.response.out.write(' <b>%s</b> '%data.msg)
                                            
            else:
                self.response.out.write('some error')
        
            self.response.out.write('and was recieved at %s<p>'%data.date)

#ask user to provide the required number
        self.response.out.write("""                  
          <form action="/no?%s" method="get">Enter Number: <input value="%s" name="num">
          <input type="submit" value="Retrieve"></form>
        </body>
      </html>""" % (urllib.urlencode({'num': num}),           
                          cgi.escape(num)))
        
        

application = webapp.WSGIApplication(
    [('/', MainPage),
     ('/report', DataUpdate),    #POST requests directed here
     ('/no',NoData)],            #specific data retrival
    debug=True)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()

